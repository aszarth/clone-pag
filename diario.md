# start project
`npx create-next-app pag`

# install svg

`yarn add @svgr/webpack`

next.config.mjs
```js
// next.config.mjs
export default {
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"],
    });

    return config;
  },
};
```