# intro
Precisava de um site para estudar/aplicar SASS, pra não ter que desenhar um no figma, decedi clonar a home da pagbank.
Dava pra fazer mais parecido copiando as fontes, usando os mesmos ícones e coisas desse tipo, mas a intenção era ter uma página com vários estilos pra aplicar o SASS não fazer um site identico.

Por esse e alguns outros motivos tiveram algumas diferenças visuais, tais como:

# diferenças visuais

<!-- <img src="./diff/footer_mobile/my1.png" alt="Descrição da Imagem" width="500" height="500"> -->

## footer PC

### minha versão:
![img](./diff/footer_pc/my1.png)
![img](./diff/footer_pc/my2.png)

### pag versão:
![img](./diff/footer_pc/pag1.png)
![img](./diff/footer_pc/pag2.png)
![img](./diff/footer_pc/pag3.png)
![img](./diff/footer_pc/pag4.png)
![img](./diff/footer_pc/pag5.png)

## home/navheader PC

### minha versão:
![img](./diff/homenavheader_pc/my1.png)

### pag versão:
![img](./diff/homenavheader_pc/pag1.png)
![img](./diff/homenavheader_pc/pag2.png)

## home/navheader MOBILE

### minha versão:
![img](./diff/homenavheader_mobile/my1.png)

### pag versão:
![img](./diff/homenavheader_mobile/pag1.png)


## footer MOBILE

### minha versão:
![img](./diff/footer_mobile/my1.png)
![img](./diff/footer_mobile/my2.png)
![img](./diff/footer_mobile/my3.png)


### pag versão:
![img](./diff/footer_mobile/pag1.png)
![img](./diff/footer_mobile/pag2.png)