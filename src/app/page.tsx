"use client";
import Icon from "@/components/Icon";
import Button from "@/components/Button";
import CheckIsMobile from "@/libs/checkIsMobile";
import Image from "next/image";
import { useEffect, useState } from "react";

export default function Home() {
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    setIsMobile(CheckIsMobile());
  }, []);

  return (
    <main
      style={{
        display: "flex",
        flexDirection: isMobile ? "column" : "row",
        justifyContent: !isMobile ? "center" : undefined,
        alignItems: isMobile ? "center" : undefined,
        paddingTop: !isMobile ? 50 : 0,
        minHeight: "100vh",
        height: "100vh",
      }}
    >
      <div
        style={{
          fontSize: !isMobile ? 28 : 12,
          fontFamily: "inherit",
          color: "#444",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
          paddingRight: !isMobile ? 60 : undefined,
          paddingBottom: !isMobile ? 150 : 20,
          paddingTop: isMobile ? 20 : undefined,
        }}
      >
        <h1>É banco,</h1>
        <h1>é máquina,</h1>
        <h1>é completo.</h1>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          paddingBottom: !isMobile ? 150 : undefined,
        }}
      >
        <BoxSelectOption
          isMobile={isMobile}
          type="primary"
          title="Para você"
          content="Os melhores CDBs do mercado, cartão de crédito grátis com 1% de cashback e todos os serviços de um banco digital completo"
          buttonTxt="Confira nossos serviços"
        />
        <BoxSelectOption
          isMobile={isMobile}
          type="secondary"
          title="Para o seu negócio"
          content="Além dos serviços Para você, as melhores maquininhas com taxa zero e a mais completa solução de pagamentos e vendas online"
          buttonTxt="Saiba mais"
          style={{
            marginTop: 25,
          }}
        />
      </div>
    </main>
  );
}

function BoxSelectOption({
  isMobile,
  type,
  title,
  content,
  buttonTxt,
  style,
}: {
  isMobile: boolean;
  type: "primary" | "secondary";
  title: string;
  content: string;
  buttonTxt: string;
  style?: object;
}) {
  const [isHovered, setIsHovered] = useState(false);
  const normalBackground =
    type === "primary"
      ? "linear-gradient(47.09deg, #54b69b .72%, rgba(166,219,217,.4) 72.48%)"
      : "linear-gradient(47.09deg, #f5de3e .72%, rgba(245,222,62,.12) 72.48%)";

  const hoverBackground =
    type === "primary"
      ? "linear-gradient(47.09deg, #54b69b .72%, rgba(84,182,155,.5) 72.48%)"
      : "linear-gradient(47.09deg, #f5de3e .72%, rgba(245,222,62,.25) 72.48%)";

  return (
    <div
      style={{
        background: isHovered ? hoverBackground : normalBackground,
        width: isMobile ? "90vw" : 650,
        height: isMobile ? 220 : 170,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        borderRadius: 15,
        padding: 20,
        ...style,
        cursor: "pointer",
      }}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <div style={{ display: "flex", flexDirection: "row" }}>
        <div style={{ display: "flex", flexDirection: "column" }}>
          <span
            style={{
              fontWeight: "bold",
              fontSize: isMobile ? 24 : 32,
              padding: "0px 10px",
            }}
          >
            {title}
          </span>
          <span
            style={{
              fontSize: isMobile ? 12 : 18,
              marginTop: 5,
              paddingLeft: 10,
              paddingRight: 50,
            }}
          >
            {content}
          </span>
        </div>
        <div
          style={{
            width: isMobile ? 90 : 150,
            height: 120,
            display: "flex",
            alignItems: "center",
          }}
        >
          <div
            style={{
              paddingRight: 30,
              width: 90,
              height: 120,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            {type == "primary" && (
              <Image
                src={`/consumer.png`}
                width={60}
                height={115}
                alt="app e cartão"
              />
            )}
            {type == "secondary" && (
              <Image
                src={`/seller.png`}
                width={90}
                height={120}
                alt="app e cartão e maquininha"
              />
            )}
          </div>
          {!isMobile && (
            <Icon name="angle-right" width={24} height={24} color="#1082be" />
          )}
        </div>
      </div>
      {isMobile && (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            marginTop: 10,
          }}
        >
          <Button type="primary" width="95%" height={40}>
            {buttonTxt}
          </Button>
        </div>
      )}
    </div>
  );
}
