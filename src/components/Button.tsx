"use client";
import { useState } from "react";

export default function Button({
  type,
  children,
  width,
  height,
  style,
  onClick,
  borderRadius,
}: {
  type: "primary" | "secondary";
  children: string;
  width: number | string;
  height: number | string;
  style?: object;
  onClick: Function;
  borderRadius?: number;
}) {
  const [borderSize, setBorderSize] = useState(1);
  const [primaryColor, setPrimaryColor] = useState("#1bb99a");
  return (
    <a
      style={{
        width: width,
        height: height,
        backgroundColor: type == "primary" ? primaryColor : "#FFF",
        color: type == "primary" ? "#FFF" : primaryColor,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        textAlign: "center",
        borderWidth: borderSize,
        borderStyle: "solid",
        borderColor: primaryColor,
        cursor: "pointer",
        fontSize: 12,
        borderRadius: borderRadius ? borderRadius : 3,
        ...style,
      }}
      onMouseEnter={() => {
        if (type == "secondary") setBorderSize(2);
        else if (type == "primary") setPrimaryColor("#12816b");
      }}
      onMouseLeave={() => {
        if (type == "secondary") setBorderSize(1);
        else if (type == "primary") setPrimaryColor("#1bb99a");
      }}
      onClick={() => {
        onClick();
      }}
    >
      {children}
    </a>
  );
}
