import Image from "next/image";
import Icon from "./Icon";

export default function NavHeaderMobile() {
  return (
    <div
      style={{
        width: "100vw",
        height: 60,
        backgroundColor: "#FFF",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        // drop shadow
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#73b743",
        boxShadow: "0px 4px 10px 0px rgba(0, 0, 0, 0.25)",
        zIndex: 10,
      }}
    >
      <div
        style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
      >
        <Icon
          name="bars"
          width={24}
          height={24}
          color="#1bb99a"
          style={{ marginLeft: 20 }}
        />
        <Image
          src="/navlogo.png"
          width={127}
          height={45}
          style={{ cursor: "pointer", marginLeft: 20 }}
          alt="logo da pag"
        />
      </div>
      <div
        style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
      >
        <Icon
          name="user-circle"
          width={24}
          height={24}
          color="#1bb99a"
          style={{ marginRight: 20 }}
        />
        <Icon
          name="shop"
          width={24}
          height={24}
          color="#1bb99a"
          style={{ marginRight: 20 }}
        />
      </div>
    </div>
  );
}
