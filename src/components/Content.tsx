import React from "react";

export default function Content({ children }: { children: React.ReactNode }) {
  return (
    <div style={{ backgroundColor: "#FEFEFE", minHeight: "100vh" }}>
      {children}
    </div>
  );
}
