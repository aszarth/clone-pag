import SvgGithub from "./svgs/github.svg";
import SvgInternet from "./svgs/internet-explorer.svg";
import SvgAngleDown from "./svgs/angle-down.svg";
import SvgAngleRight from "./svgs/angle-right.svg";
import SvgAngleUp from "./svgs/angle-up.svg";
import SvgFacebook from "./svgs/facebook-f.svg";
import SvgInstagram from "./svgs/instagram.svg";
import SvgYoutube from "./svgs/youtube.svg";
import SvgLinkedin from "./svgs/linkedin-in.svg";
import SvgBars from "./svgs/bars.svg";
import SvgUserCircle from "./svgs/user-circle.svg";
import SvgShop from "./svgs/shopping-cart.svg";

export default function Icon({
  name,
  width,
  height,
  color,
  style,
}: {
  name:
    | "internet"
    | "git"
    | "angle-down"
    | "angle-right"
    | "angle-up"
    | "facebook"
    | "instagram"
    | "youtube"
    | "linkedin"
    | "bars"
    | "user-circle"
    | "shop";
  width: number;
  height: number;
  color: string;
  style?: object;
}) {
  return (
    <div style={{ width: width, height: height, ...style }}>
      {name == "internet" && (
        <SvgInternet fill={color} width={width} height={height} />
      )}
      {name == "git" && (
        <SvgGithub fill={color} width={width} height={height} />
      )}
      {name == "angle-down" && (
        <SvgAngleDown fill={color} width={width} height={height} />
      )}
      {name == "angle-right" && (
        <SvgAngleRight fill={color} width={width} height={height} />
      )}
      {name == "angle-up" && (
        <SvgAngleUp fill={color} width={width} height={height} />
      )}
      {name == "facebook" && (
        <SvgFacebook fill={color} width={width} height={height} />
      )}
      {name == "instagram" && (
        <SvgInstagram fill={color} width={width} height={height} />
      )}
      {name == "youtube" && (
        <SvgYoutube fill={color} width={width} height={height} />
      )}
      {name == "linkedin" && (
        <SvgLinkedin fill={color} width={width} height={height} />
      )}
      {name == "bars" && <SvgBars fill={color} width={width} height={height} />}
      {name == "user-circle" && (
        <SvgUserCircle fill={color} width={width} height={height} />
      )}
      {name == "shop" && <SvgShop fill={color} width={width} height={height} />}
    </div>
  );
}
