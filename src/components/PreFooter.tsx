"use client";
import { useState } from "react";
import Icon from "./Icon";

export default function PreFooter({ isMobile }: { isMobile: boolean }) {
  const [manySomeNavItemOpen, setManySomeNavItemOpen] = useState(0);
  return (
    <div
      style={{
        backgroundColor: "#272B2D",
        height: isMobile ? 1000 + manySomeNavItemOpen * 400 : 800,
        width: "100vw",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <PrefooterNavItems
        isMobile={isMobile}
        manySomeNavItemOpen={manySomeNavItemOpen}
        setManySomeNavItemOpen={setManySomeNavItemOpen}
      />
      <div style={{ marginTop: !isMobile ? 50 : 10 }}>
        <div
          className="contactCardsContainer"
          style={{
            display: "flex",
            flexDirection: !isMobile ? "row" : "column",
            alignItems: isMobile ? "center" : undefined,
          }}
        >
          <ContactCard
            title="Capitais e regiões metropolitanas"
            number="4003-1775"
            style={{ marginTop: isMobile ? 25 : undefined }}
          />
          <ContactCard
            title="Demais localidades"
            number="0800-728-2174"
            style={{
              marginLeft: !isMobile ? 10 : 0,
              marginTop: isMobile ? 25 : 0,
            }}
          />
          <ContactCard
            title="Denúncias"
            number="3004-4770"
            content="Disponível 24h / 7 dias por semana"
            style={{
              marginLeft: !isMobile ? 10 : 0,
              marginTop: isMobile ? 25 : 0,
            }}
          />
          <ContactCard
            title="Ouvidoria"
            number="0800-728-2167"
            content="Seg. a sexta: 9h às 18h
            (exceto feriados)"
            style={{
              marginLeft: !isMobile ? 10 : 0,
              marginTop: isMobile ? 25 : 0,
            }}
          />
        </div>
        <div
          className="endBeforeFooter"
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            marginTop: 50,
            marginBottom: 10,
          }}
        >
          <div
            className="socialMediaContainer"
            style={{ display: "flex", flexDirection: "row" }}
          >
            <SocialMediaBall
              name="facebook"
              href="https://www.facebook.com/pagbank"
              isMobile={isMobile}
            />
            <SocialMediaBall
              name="instagram"
              href="https://www.instagram.com/pagbank/?hl=pt"
              style={{ marginLeft: 15 }}
              isMobile={isMobile}
            />
            <SocialMediaBall
              name="youtube"
              href="https://www.youtube.com/@PagBank"
              style={{ marginLeft: 15 }}
              isMobile={isMobile}
            />
            <SocialMediaBall
              name="linkedin"
              href="https://www.linkedin.com/company/pagbank"
              style={{ marginLeft: 15 }}
              isMobile={isMobile}
            />
          </div>
          <HelpFooterButton />
          <hr
            className="separatorPreFooter"
            style={{
              width: "80%",
              height: 3,
              backgroundColor: "#000",
              marginTop: 15,
            }}
          />
          <span
            className="securityWarningPreFooter"
            style={{
              marginTop: 15,
              fontSize: 14,
              color: "#999",
              textAlign: "center",
            }}
          >
            Todas as páginas do PagBank estão em ambiente de segurança.
          </span>
        </div>
      </div>
    </div>
  );
}

function PrefooterNavItems({
  isMobile,
  manySomeNavItemOpen,
  setManySomeNavItemOpen,
}: {
  isMobile: boolean;
  manySomeNavItemOpen: number;
  setManySomeNavItemOpen: Function;
}) {
  return (
    <>
      <div
        style={{
          display: "flex",
          flexDirection: isMobile ? "column" : "row",
          paddingTop: 20,
        }}
      >
        <PreFooterItem
          title="Para seu negócio"
          items={[
            "Maquininhas",
            "Cartão + Maquininhas",
            "Compartilhamento",
            "Venda Online",
            "Vouncher",
            "PlugPag",
            "TEF",
            "PagTotem",
            "Folha de Pagamento",
            "Indique e Ganhe",
            "App PagVendas",
            "Manuais das Maquininhas",
            "ClubePag",
            "Pagamentos Internacionais",
            "Seja um Parceiro PagBank",
          ]}
          isMobile={isMobile}
          manySomeNavItemOpen={manySomeNavItemOpen}
          setManySomeNavItemOpen={setManySomeNavItemOpen}
        />
        <PreFooterItem
          title="Para Você"
          items={[
            "Conta Digital PagBank",
            "Conta PJ PagBank",
            "Conta Renderia",
            "Investimentos",
            "Cartões PagBank",
            "Pix",
            "Seguros e PagBank Saúde",
            "Recarga",
            "Shopping",
            "Compra Protegida",
            "Pagamento - IPVA",
            "Recargua Bilhete Único",
            "Recarga de Games",
          ]}
          isMobile={isMobile}
          manySomeNavItemOpen={manySomeNavItemOpen}
          setManySomeNavItemOpen={setManySomeNavItemOpen}
        />
        <PreFooterItem
          title="Produtos para"
          items={[
            "Autônomos e MEI",
            "Comércio e Lojas Físicas",
            "Restaurantes, Bares e Lanchonetes",
          ]}
          isMobile={isMobile}
          manySomeNavItemOpen={manySomeNavItemOpen}
          setManySomeNavItemOpen={setManySomeNavItemOpen}
        />
        <PreFooterItem
          title="Informações"
          items={[
            "Sobre o PagBank",
            "Site Developers",
            "Sandbox",
            "Contrato de Serviços",
            "Regras de Uso",
            "Regras de compartilhamento",
            "Segurança e privacidade",
            "Serviço de Gestão de Pagamentos",
            "Dicas de Segurança Online",
            "Investidores",
            "Mapa de Site",
          ]}
          isMobile={isMobile}
          manySomeNavItemOpen={manySomeNavItemOpen}
          setManySomeNavItemOpen={setManySomeNavItemOpen}
        />
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: isMobile ? "column" : "row",
          paddingTop: !isMobile ? 20 : 0,
        }}
      >
        <PreFooterItem
          title="Regulatório"
          items={[
            "CVM",
            "Atendimento CVM",
            "Atendimento MRP",
            "BSM",
            "BJ",
            "BancoSeguro",
            "Investimentos",
          ]}
          isMobile={isMobile}
          manySomeNavItemOpen={manySomeNavItemOpen}
          setManySomeNavItemOpen={setManySomeNavItemOpen}
        />
        <PreFooterItem
          title="Conteúdo"
          items={["Blog", "Materiais de download", "Quiz de Maquininhas"]}
          isMobile={isMobile}
          manySomeNavItemOpen={manySomeNavItemOpen}
          setManySomeNavItemOpen={setManySomeNavItemOpen}
        />
        <PreFooterItem
          title="Institucional"
          items={[
            "Demonstrações Financeiras",
            "Política de Segurança Cibernética",
            "Política de Continuidade de Negócios",
            "Relatório de Ouvidoria",
            "Política de Troca",
            "Estrutura de Risco",
          ]}
          isMobile={isMobile}
          manySomeNavItemOpen={manySomeNavItemOpen}
          setManySomeNavItemOpen={setManySomeNavItemOpen}
        />
        <PreFooterItem
          title="Contato"
          items={[
            "Perguntas frequentes",
            "Comunicar perda ou roubo de celular",
            "Contato comercial",
            "Envie um e-mail",
            "Carreiras",
            "Acessoria de impresa",
            "Canal de Denúncia PagBank",
          ]}
          isMobile={isMobile}
          manySomeNavItemOpen={manySomeNavItemOpen}
          setManySomeNavItemOpen={setManySomeNavItemOpen}
        />
        {/* <PreFooterItem title="Canais de atendimento" items={[""]} isMobile={isMobile} /> */}
      </div>
    </>
  );
}

function PreFooterItem({
  isMobile,
  title,
  items,
  manySomeNavItemOpen,
  setManySomeNavItemOpen,
}: {
  isMobile: boolean;
  title: string;
  items: string[];
  manySomeNavItemOpen: number;
  setManySomeNavItemOpen: Function;
}) {
  const [openList, setOpenList] = useState(false);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        marginLeft: !isMobile ? 10 : undefined,
        width: !isMobile ? 200 : "90vw",
        marginTop: isMobile ? 25 : 0,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: isMobile ? "space-between" : undefined,
          cursor: "pointer",
        }}
        onClick={() => {
          if (!openList) {
            setManySomeNavItemOpen(manySomeNavItemOpen + 1);
          } else {
            setManySomeNavItemOpen(manySomeNavItemOpen - 1);
          }
          setOpenList(!openList);
        }}
      >
        <span
          style={{
            fontSize: !isMobile ? 14 : 18,
            fontWeight: "bold",
            paddingBottom: 5,
            color: "#FFF",
          }}
        >
          {title}
        </span>
        {isMobile && (
          <Icon
            name="angle-down"
            width={24}
            height={24}
            color="#FFF"
            style={{ marginLeft: 2 }}
          />
        )}
      </div>
      {(!isMobile || openList) && (
        <>
          {items.map((e) => {
            return (
              <span
                key={e}
                style={{
                  fontSize: !isMobile ? 10 : 14,
                  marginTop: !isMobile ? 5 : 10,
                  color: "#FFF",
                  cursor: "pointer",
                }}
              >
                {e}
              </span>
            );
          })}
        </>
      )}
    </div>
  );
}

function ContactCard({
  title,
  number,
  content,
  style,
}: {
  title: string;
  number: string;
  content?: string;
  style?: object;
}) {
  return (
    <div
      style={{
        color: "#FFF",
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        width: 200,
        textAlign: "center",
        ...style,
      }}
    >
      <span style={{ fontSize: 14, fontWeight: "bold" }}>{number}</span>
      <span style={{ fontSize: 16 }}>{title}</span>
      {content && <span style={{ fontSize: 12 }}>({content})</span>}
    </div>
  );
}

function HelpFooterButton() {
  const [isHover, setIsHover] = useState(false);
  return (
    <div
      style={{
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: !isHover ? "#FFF" : "#272B2D",
        backgroundColor: !isHover ? "#272B2D" : "#FFF",
        color: !isHover ? "#FFF" : "#272B2D",
        width: 90,
        height: 30,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        cursor: "pointer",
        marginTop: 20,
      }}
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
    >
      Ajuda
    </div>
  );
}

function SocialMediaBall({
  name,
  href,
  style,
  isMobile,
}: {
  name: "facebook" | "instagram" | "youtube" | "linkedin";
  href: string;
  style?: object;
  isMobile: boolean;
}) {
  const [isHover, setIsHover] = useState(false);
  return (
    <div
      style={{
        borderWidth: 2,
        borderStyle: "solid",
        borderColor: !isHover ? "#FFF" : "#272B2D",
        backgroundColor: !isHover ? "#272B2D" : "#FFF",
        width: isMobile ? 48 : 32,
        height: isMobile ? 48 : 32,
        borderRadius: isMobile ? 24 : 16,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        cursor: "pointer",
        ...style,
      }}
      onMouseEnter={() => {
        setIsHover(true);
      }}
      onMouseLeave={() => {
        setIsHover(false);
      }}
      onClick={() => {
        window.open(href, "_blank");
      }}
    >
      <Icon
        name={name}
        width={isMobile ? 24 : 16}
        height={isMobile ? 24 : 16}
        color={!isHover ? "#FFF" : "#272B2D"}
      />
    </div>
  );
}
