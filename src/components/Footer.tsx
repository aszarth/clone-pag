import Image from "next/image";
import PreFooter from "./PreFooter";

export default function Footer({ isMobile }: { isMobile: boolean }) {
  return (
    <div>
      <PreFooter isMobile={isMobile} />
      <div
        style={{
          backgroundColor: "#1B1E20",
          height: 200,
          width: "100vw",
          textAlign: "center",
          color: "#999999",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          fontSize: 12,
          paddingRight: "10%",
          paddingLeft: "10%",
        }}
      >
        <Image
          src="/footerlogo.png"
          width={145}
          height={56}
          alt="logo da pag"
        />
        <span style={{ marginTop: 10 }}>
          © 1996-2024 Todos os direitos reservados.
        </span>
        <span style={{ marginTop: 5 }}>
          PAGSEGURO INTERNET INSTITUIÇÃO DE PAGAMENTO S/A - CNPJ/MF
          08.561.701/0001-01 Av. Brigadeiro Faria Lima, 1.384, São Paulo - SP -
          CEP 01451-001
        </span>
      </div>
    </div>
  );
}
