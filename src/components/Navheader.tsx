"use client";
import Image from "next/image";
import Button from "./Button";
import { useState } from "react";
import Icon from "./Icon";

// pro dropbox
// boxShadow: "0 8px 16px rgba(0, 0, 0, 0.2)",
// boxShadow: "0 10px 20px rgba(0, 0, 0, 0.3)",

export default function NavHeader() {
  return (
    <div
      style={{
        width: "100%",
        height: 90,
        backgroundColor: "#FFF",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        // Sombra forte com z-index
        boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
        zIndex: 10,
        position: "relative",
      }}
    >
      <div
        className="navContainerLogoAndItems"
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <div
          className="navLogo"
          style={{
            width: 150,
            height: 75,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Image
            src="/navlogo.png"
            width={127}
            height={45}
            style={{ cursor: "pointer" }}
            alt="logo da pag"
          />
        </div>
        <div
          className="navItems"
          style={{ display: "flex", flexDirection: "row" }}
        >
          <ItemNavHeader title="Para Seu Negócio" />
          <ItemNavHeader title="Para Você" />
          <ItemNavHeader title="Televendas" />
          <ItemNavHeader title="Ajuda" />
        </div>
      </div>
      <div
        className="navButtons"
        style={{ display: "flex", flexDirection: "row", marginRight: 50 }}
      >
        <Button
          type="primary"
          width={96}
          height={32}
          style={{ marginLeft: 20 }}
        >
          Entrar
        </Button>
        <Button
          type="secondary"
          width={96}
          height={32}
          style={{ marginLeft: 20 }}
        >
          Criar Conta
        </Button>
        <Icon
          name="shop"
          width={32}
          height={32}
          color="#1bb99a"
          style={{ marginLeft: 20, cursor: "pointer" }}
        />
      </div>
    </div>
  );
}

function ItemNavHeader({ title }: { title: string }) {
  const [fontColor, setFontColor] = useState("#000");
  return (
    <div
      style={{
        marginLeft: 30,
        cursor: "pointer",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
      }}
      onMouseEnter={() => {
        setFontColor("#1bb99a");
      }}
      onMouseLeave={() => {
        setFontColor("#000");
      }}
    >
      <span style={{ fontSize: 16, color: fontColor }}>{title}</span>
      <Icon
        name="angle-down"
        color="#1bb99a"
        width={16}
        height={16}
        style={{ marginLeft: 5 }}
      />
    </div>
  );
}
